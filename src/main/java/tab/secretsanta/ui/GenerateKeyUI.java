package tab.secretsanta.ui;

import tab.secretsanta.SantaCipher;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Created by andrey.tesluk on 28.11.2014.
 */
public class GenerateKeyUI {

    public static void main(String[] args) throws NoSuchAlgorithmException {

        KeyPair key = SantaCipher.generateKeyPair();

        String privateKey = Base64.getEncoder().encodeToString(key.getPrivate().getEncoded());
        String publicKey = Base64.getEncoder().encodeToString(key.getPublic().getEncoded());

        System.out.println("Your public key:");
        System.out.println(publicKey);
        System.out.println("Do not afraid to share it with friends =)\n");

        System.out.println("Your private key:");
        System.out.println(privateKey);
        System.out.println("Keep it private! Cause... it's PRIVATE!");

    }

}
