package tab.secretsanta.ui;

import tab.secretsanta.SantaCipher;

import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by andrey.tesluk on 28.11.2014.
 */
public class WhoIsMineUI {

    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException, NoSuchPaddingException, IOException {
        // Place here all encrypted text
        String encryptedText = "GvE3Usv3X5+ddZ89wRD0xUr9IGpkWtnljkOsU2vErFcSPc9Cpr2a/MWr+XVXjiUOsyI1G/DmAk+jGvBgy9QeWBb8J5SSx53aUMiC3vTwAyucvu2H4MBB5ln18BT/I/Cr18PxXUmZHgTi+fBhPL+tZdbovWjcKo8TqzQLwvfLMno=\n" +
                "dYbiwO9oanDQ4DoicxJeF4V37zjXyDrwzn9WVPWZiB5XolxVA/Bw6bTE6rX8nTW9UX/z+PJxCCeRa32AgE89W5tBJ23DZFHpTW8NBLiwUyT13LphPh4s8KbHhWMMP1Gtf73BKOrF8JK8HpFX3xOksotot62Myeo/3btSkihbn6s=\n" +
                "Di5vlkSUsSKCuH43wP8lxN9BwLKblTrUKfZXlTb8StVuKdvapVHEY2wMjt/H79o2Sw4q0V77IAlKxzCqKp2kvMe+hcLAaji3Z0HGo0GdtCuwNWRHVOOcStwa2lqCSq5Z95SvDCc5fzn8Hi1kc0wtSmN3kQZJyFNjLWiJuCIhUaQ=\n" +
                "PzpZ2vaOBnxGp8Gk2nQJK8dfj7S/UOeMDtkFM1aPxeHF5vAYpSLm2dVUx+A/ZOpSjBmG4zQrCWwWplSJIQ6el7fppXlYC3pS90LZe/bZR4tNy7Guau1Dh/6SkoUI1kul2OrKETV/WE7D6cVpUUztkpgCQbCWrBePzZtoL1+Ok7o=\n" +
                "rbD8wx4QdYB/dbmOYCCcvIh85yWA8W+HaAIJTwVJ9GcOOg0+OCrp7CyIwLb3F+Nfnc2OtT8TVm+iHwjoHntElEjlJ65o5yM5qj4Ohp9cbrS+Jam3jR2qU+GuF5WiPHwOBSwKHJ/TV3ZO/DWQ3IKvIW+dsvrZpdTtags3ge2FqIw=\n" +
                "cYJvh0klvYd1cpZaAREkRPpJ68FrM+xSAba/Pfp1gKgfGwz0bqAL+JwxWSQqwETan0+dqwfO+gUP64N01qygDa9aUT6Eqe7WCs9I4/ZP7IbT9tXhNDdy3C4MP9cZ3R6dN0r3w4xqUUWyNUAdgqcdB3OJMwg/RQDAzEj42vl1tyI=\n" +
                "OPOBNlcKOaYVCpP/fdCKfPTiPLDcn8GOHMoBB8BbSHJV+i9SxSYrbxCL7JGKYLsl+3vexz47bFoTAyGdTJu/rVGzqOhvhfSdcDsdTmrpDcvwhjLN+yWa84NkDz34u9/ocgAeowaRZyQSZSz7JEWIuMPmRB2M7pyxSl+YzTSTz/M=\n" +
                "DgpWQl9h/kWcy79h7vp8tqMXU+B2yG4j7Pgd+UfoMfCw5IqFzMVLohtE7qJstzNpO7rk8APMNjN/3Z5wfjxcakIVt29LkPSid6kwj3sEc+RMzpLZdM4QITaXa821vmEDT/wQxmbhUfNerwn539cwFC/Z9z7GR91uZJtrhADkBqU=";

        String myPrivateKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAIB9lg/eRC/FqCTca23GQGHtwMHkmaCPx1SCo97BEdWqRV+7hG7+HG8RbVHe95043IMw1JChLhK1Mc9jFYppBuJ8JG5jEwi4hiC1Ape+x+ngZOoEO5DEAKvySwbvc8qsUjqV+KcAdt7ZImvlgR2vkZjcfKC0v/UXG5sO3vPCkj6LAgMBAAECgYBlY7P//YGMuMl/nLbgvmmbXMKXvU1El2/Qze2FHpB2K4/mhM9xze6d7gB3dBur4myO0P9KyQTUGkqdvTAIMewNuXEyLMlldnG+6SxKqcPtfp2ysAwmBydUcwiir9SITfPhY0SO5s6JyObgDmPv5IKGczUooxqKlmFEOD079hwnMQJBALW5TgyNiwTPddHUjGYenyU1iAbucV09zWLavoE8rKE0cLVbRDUvdukXniCdxBBYXhSTXskDJzY7z7vJkvQYrj8CQQC1AjYjomtbYl33DehjEuU8xKrmFxCiFr1IVkP5Ez8tqJ4/6IzsnfbOYwNxpNEC3HdQhYycF1jHhPufRXlsNfS1AkEAplLPM8ANAD8deAqQ4hqe0ICZWra8fXnGVPceq8yrt/P2QJci64bgz+C08udS/x7SJQTpGRPQvNurexW4cb9xIwJAZJm+DUAP+tJI9y47ohS2Y3fKo+bdz7hjqviG/r+7tVyp10sgR8HjkgFlwJuhrr1pcs9Gvtoa9YjrcDHkskeqoQJBAKyAOK8gCkuweNIa8ENXbvnf8zbzCK8IQgQdysO3Yu987YrL1jUz7ep6vGch0ibH27mrXmMduFbhoVJW+i2M+NI=";

        String[] encryptedRows = encryptedText.split("\n");

        for(int i=0;i<encryptedRows.length;i++){
            String decrypted = SantaCipher.decrypt(encryptedRows[i], myPrivateKey);
            System.out.println(String.format("%d) %s", i, decrypted));
        }

    }

}
