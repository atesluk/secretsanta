package tab.secretsanta.ui;

import tab.secretsanta.HappyUser;
import tab.secretsanta.SantaCipher;
import tab.secretsanta.SecretSanta;

import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * Created by andrey.tesluk on 28.11.2014.
 */
public class ShufflerUI {

    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException, NoSuchPaddingException, IOException {
        List<HappyUser> users = new ArrayList<HappyUser>();
        users.add(new HappyUser("Теслюк Андрій", "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCAfZYP3kQvxagk3GttxkBh7cDB5Jmgj8dUgqPewRHVqkVfu4Ru/hxvEW1R3vedONyDMNSQoS4StTHPYxWKaQbifCRuYxMIuIYgtQKXvsfp4GTqBDuQxACr8ksG73PKrFI6lfinAHbe2SJr5YEdr5GY3HygtL/1FxubDt7zwpI+iwIDAQAB"));
        users.add(new HappyUser("Теслюк Таня", Base64.getEncoder().encodeToString(SantaCipher.generateKeyPair().getPublic().getEncoded())));
        users.add(new HappyUser("Гриценко Женя", Base64.getEncoder().encodeToString(SantaCipher.generateKeyPair().getPublic().getEncoded())));
        users.add(new HappyUser("Степанов Владислав", Base64.getEncoder().encodeToString(SantaCipher.generateKeyPair().getPublic().getEncoded())));
        users.add(new HappyUser("Потапський Богдан", Base64.getEncoder().encodeToString(SantaCipher.generateKeyPair().getPublic().getEncoded())));
        users.add(new HappyUser("Давыдов Александр", Base64.getEncoder().encodeToString(SantaCipher.generateKeyPair().getPublic().getEncoded())));
        users.add(new HappyUser("Александра Усова", Base64.getEncoder().encodeToString(SantaCipher.generateKeyPair().getPublic().getEncoded())));
        users.add(new HappyUser("Пилипенко Сергій", Base64.getEncoder().encodeToString(SantaCipher.generateKeyPair().getPublic().getEncoded())));

//        users.add(new HappyUser("Теслюк Андрій","MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAK+/W3Yj2Z4jMB+MO1xrpWq4zxu4EyPXqz032LCB2uKwZPEB4VonkC8Ds4v5niOoVA2K9E67d3TqLpiaQFkPvT0CAwEAAQ=="));
//        users.add(new HappyUser("Теслюк Таня","MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIBd0EXgEfU1b/AHtpdLc6OTadINsTDOCu+nzcowfajTVUSIZdxfjq5cglvl9X5M+s2FLNUTFK3suSreA31QdWECAwEAAQ=="));
//        users.add(new HappyUser("Гриценко Женя","MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIQGE3YBjzHO2O+RjGkFTXiVx4obJ316IRswjKUBiWQIH3UroL2IlNg3LWAocI65QpSWvwXhQfQDUIH3Za7D/mECAwEAAQ=="));
//        users.add(new HappyUser("Степанов Владислав","MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJZP0OcKyYCz+HGaoh8w4FTY25+CDNMF0ktHLHFTAaXAQCy0IAlBQzfwtBC+ev5a/jf+bm/VJv9mrgF6Ig54Q3sCAwEAAQ=="));
//        users.add(new HappyUser("Потапський Богдан","MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKVCH4STcosLllTty9xR2iUB0icTlpDj259aCDfEF6chJ+NN03HmMlq3dDi9DBv1xrIPxzZRKyXp/v0Jqj8C0mcCAwEAAQ=="));
//        users.add(new HappyUser("Давыдов Александр","MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAMA9zfHaxjpo8yTmBO4DSOBAWWGn4uJUoeAZ0nG11HdDC4MiElVq3Mx5n9CF27SunGKMH9efyg5sA+0EleOCL/kCAwEAAQ=="));
//        users.add(new HappyUser("Александра Усова","MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALTTNbqhSt4yLhUrA10K+EYb0OkSqX3tRd35RsYCNDBz9mMUr/GuALxbIIe2g7gLDUePbloa8Tus1hkrmfiuCqMCAwEAAQ=="));
//        users.add(new HappyUser("Пилипенко Сергій","MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKXfWY+ebk71rBjy4r8VHGCSxrcOkYAJWYsvgjqY1Fv9NIFOL9APDDZ5OMRX3UaPiHb4+thMocfaky+2G9ieARECAwEAAQ=="));
//        users.add(new HappyUser("Чевокин Сергей",""));
//        users.add(new HappyUser("Смаковська Анастасія",""));
//        users.add(new HappyUser("Cкрынникова Анастасия","MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIqgKtDrbN20VDuwkwSXpgjXe2Z9c9QhZP+hc2nww2w8F85sSolSxgF1WoeHLovOyqamHdprHNJ1tXiJpGFy+iMCAwEAAQ=="));
//        users.add(new HappyUser("Колесник Аня","MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJincmajNew0Cm8PfH7ViM4o5osjvkfKr+RkJhKwN4xec8/3bm+LKjfwjGuhYwK81Pp2QkzfgU2OQ1+iOZgfBVcCAwEAAQ=="));
//        users.add(new HappyUser("Андрей Усов","MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJpLD6gpgAc15cwE/Kd+KgjtCFG7Le6KAGZgmLeSaGVIX+IH5BG4RnyTyk8eXMmYlX/ZUDXeTwSNJoBHx5kvLVsCAwEAAQ=="));

        List<String> encrypted = SecretSanta.get(users);

        for (String str : encrypted) {
            System.out.println(str);
        }
    }
}
