package tab.secretsanta;

import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * Created by andrey.tesluk on 18.11.2014.
 */
public class Just4Test {

    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException, NoSuchPaddingException, IOException {
        String pub = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC1jYg5+gQl9iiftzG1a6hG+PWdMBx6nXTemAVvEIBhTpBfamGuvCfRSHjlla" +
                "De2Ps/BdK19Xd/XMJ8c6qrjoswQ3LcZm5q7yVQyAw2Hh4LcKPwLMTg2ql5czJyfbXNeVYm3ro7ux9tc9kWU3FLIrSMM+X8fdVXhiCAw" +
                "964y+rnWQIDAQAB";
        String priv = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALWNiDn6BCX2KJ+3MbVrqEb49Z0wHHqddN6YBW8QgGFOkF9qY" +
                "a68J9FIeOWVoN7Y+z8F0rX1d39cwnxzqquOizBDctxmbmrvJVDIDDYeHgtwo/AsxODaqXlzMnJ9tc15Vibeuju7H21z2RZTcUsitIwz" +
                "5fx91VeGIIDD3rjL6udZAgMBAAECgYATBIVdlVvz4go1WMpDN8jDznZkBdMl4PKKEqCXFsJr7v/ejtU2qYz+Njo7ymW0kkQXQPPmjZT" +
                "76WqSAMfC+saHlW/mzWkmDIL4d3S9xgSFr91NfS3iyV2MiHYr7XRN0B7CF9wiAqZOHutgvrbr7i7ufgjx6Z32Tl31c7YZIlmlEQJBAO" +
                "fjsUxU0aBC+mVPIjzzyJ0fxqjqigSoq7MoXPhZb5ABDgZSBeO/jKHOGmjeMpPdJluEaguz5sRfrCoQq+Q0gNUCQQDIbgISFZd0ztXUP" +
                "lF+HDansz88zODp52AT/daWniMzQiU9dMg3DOpIrtn6eQsEf5culKmB+9q5dwB3g6lSju51AkBv/Zwr9C7FSnOpp90uOMJjd+w2ugbs" +
                "FxXNUaoM2wyxLIE8djKX+nON1X8VtHISzVRwEHQysLC7mEUKj8CkVuL9AkEArKX+SJ+qwGau0M4BmrUo61o/HduJ3lJ9c5sTrth3j97" +
                "pfz/Ke4k3gvaDM/vyXqSdkPVj8mEFbv8wOsMCJuiSRQJBALfgCF83jdixk+hRHWkERKyIMWy0UiecLxUU/nkoNxFeBPeAUmDctibLl4" +
                "tX7bdEWBRnFqhhZy4fGm2a4iSskQ4=";

        List<HappyUser> users = new ArrayList<HappyUser>();
        users.add(new HappyUser("Миколка", pub));
        users.add(new HappyUser("Едуард", Base64.getEncoder().encodeToString(SantaCipher.generateKeyPair().getPublic().getEncoded())));
        users.add(new HappyUser("Генадій", Base64.getEncoder().encodeToString(SantaCipher.generateKeyPair().getPublic().getEncoded())));
        users.add(new HappyUser("Галина", Base64.getEncoder().encodeToString(SantaCipher.generateKeyPair().getPublic().getEncoded())));
        users.add(new HappyUser("Людмила", Base64.getEncoder().encodeToString(SantaCipher.generateKeyPair().getPublic().getEncoded())));

        List<String> encrypted = SecretSanta.get(users);

        for (String str : encrypted) {
            System.out.println(str);
        }

        System.out.println();

        for (int i = 0; i < encrypted.size(); i++) {
            System.out.print(i + ") ");
            System.out.println(SantaCipher.decrypt(encrypted.get(i), priv));
        }
    }


}
